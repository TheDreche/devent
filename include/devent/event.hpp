/* Copyright (C) 2021 Dreche
 * 
 * This file is part of libdevent.
 * 
 * libdevent is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdevent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdevent.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEVENT_EVENT_HPP
#define DEVENT_EVENT_HPP

/*!
 * @file
 * @brief Defines the event class.
 */

#include "export.hpp"

namespace devent {
	class LIBDEVENT_EXPORT event;
}

namespace devent {
	/*!
	 * @brief An event.
	 * 
	 * Each time an event is happening, a new instance of a subclass should be created.
	 * 
	 * All event types should inherit from this.
	 */
	class event {};
}

/*!
 * @brief Define a new event type.
 * 
 * Usage example:
 * 
 * 	DEVENT_EVENT(handle_setting_force_changed, handle_changed) {
 * 		bool new_force;
 * 	};
 * 
 * In case you are interested: Behind the scenes this expands to
 * 
 * 	struct handle_setting_force_changed : public handle_changed {
 * 		bool new_force;
 * 	};
 * 
 * But that is just an implementation deatail.
 * 
 * @param name The name of the event type.
 * @param specialof This is a special type of that event. Use devent::event if it is a general event.
 */
#define DEVENT_EVENT(name, specialof) struct name : public specialof

#endif
