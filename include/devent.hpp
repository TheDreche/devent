/* Copyright (C) 2021 Dreche
 * 
 * This file is part of libdevent.
 * 
 * libdevent is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * libdevent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libdevent.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEVENT_HPP
#define DEVENT_HPP

/*!
 * @file
 * @brief Includes the most commonly used parts of the puplic API of libdevent.
 */

/*!
 * @brief Contains all libdevent related classes.
 */
namespace devent {}

#include "devent/event.hpp"
#include "devent/export.hpp"

#endif
