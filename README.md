# DEVENT

[![pipeline status](https://shields.io/gitlab/pipeline/TheDreche/devent/main?logo=gitlab)](https://gitlab.com/TheDreche/devent/-/pipelines) [![code coverage](https://img.shields.io/gitlab/coverage/TheDreche/devent/main?logo=gitlab)](https://gitlab.com/TheDreche/devent) [![lines of code](https://shields.io/tokei/lines/gitlab/TheDreche/devent)](https://gitlab.com/TheDreche/devent/)

This is a library providing an implementation of an event system.

Currently not finished.

## Building

Two build systems are supported:

- GNU autoconf, automake, ...
- CMake

GNU autoconf, automake, … is prioritized more. For building and installing using them, see [INSTALL](./INSTALL).

## Documentation
The source code is documented using [doxygen](https://www.doxygen.nl/). Pass the `--enable-doc` option to `./configure` to generate all documentation. (For more details, see Configure options)

## Versioning
This project uses [semantic versioning](https://semver.org).

## License
Copyright (C) 2021 Dreche

libdevent is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

libdevent is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libdevent.  If not, see <http://www.gnu.org/licenses/>.

For more information, see the full license text: [COPYING](COPYING)

## Other information
* Until final 0.0.0, the changelog and news won't be written.

