# AX_CXX_OVERRIDE
# ---------------
AC_DEFUN([AX_CXX_OVERRIDE],
[AC_CACHE_CHECK([whether the override keyword exists], ax_cv_cxx_override,
[AC_COMPILE_IFELSE([AC_LANG_PROGRAM([],
[[
class a {
	public:
		virtual int mynum() {
			return 1;
		}
}

class b : public a {
	public:
		virtual int mynum() override {
			return 2;
		}
}

int main() {
	b v;
	(void) v.mynum();
	return 0;
}
]])],
		   [ax_cv_cxx_override=no],
		   [ax_cv_cxx_override=yes])])
if test $ax_cv_cxx_override = no; then
  AC_DEFINE(override,,
	    [Define to empty if `override' does not exist.])
fi
])# AX_CXX_OVERRIDE

